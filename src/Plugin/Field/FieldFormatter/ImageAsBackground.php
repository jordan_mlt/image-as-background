<?php

namespace Drupal\image_as_background\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;


/**
 * Plugin implementation of the 'image_as_background' formatter.
 *
 * @FieldFormatter(
 *   id = "image_as_background",
 *   label = @Translation("Background"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class ImageAsBackground extends ImageFormatter
{
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    foreach ($elements as &$element) {
      $element['#theme'] = 'image_as_background_formatter';
    }

    return $elements;
  }
}